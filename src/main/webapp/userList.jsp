<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/directive/lang.jspf" %>
<%@ page isELIgnored="false" %>
<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">

    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ECE9E0;
            color: #656665;
            border: 5px solid #ECE9E0;
            border-radius: 15px;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #F5D7BF;
            padding: 5px;
        }
    </style>

</head>
<body>

<%@ include file="/WEB-INF/directive/header.jspf" %>


<c:forEach var="user" items="${userList}">
            <table border="1" align="center" width="70%">
                <tr>
                    <td width="15%"><c:out value="${user.id}"/></td>
                    <td width="15%"><c:out value="${user.login}"/></td>
                    <td width="15%"><c:out value="${user.email}"/></td>
                    <td width="15%"><c:out value="${user.role.role}"/></td>
                    <td width="15%"><c:out value="${user.isBanned}"/></td>
                    <td width="10%">
                    <form action="userList" method="post">
                    <input type="hidden" name="userId" value ="${user.id}">
                    <input type="hidden" name="isBanned" value ="${user.isBanned}">
                    <input type="submit" value="Block/Unblock user">
                    </form>
                    </td>
                </tr>
            </table>
</c:forEach>





</body>
</html>
