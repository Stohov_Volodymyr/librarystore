<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/WEB-INF/directive/lang.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <link rel="stylesheet" href="style.css">

    <style type="text/css">
                table {
                    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
                    text-align: left;
                    font-size: 20px;
                    border-collapse: separate;
                    border-spacing: 5px;
                    background: #ECE9E0;
                    color: #656665;
                    border: 5px solid #ECE9E0;
                    border-radius: 15px;
                }

                th {
                    font-size: 18px;
                    padding: 10px;
                }

                td {
                    background: #F5D7BF;
                    padding: 5px;
                }
            </style>

</head>
<body>


<%@ include file="/WEB-INF/directive/header.jspf" %>


<div class="form">
    <h1>Library</h1>
    <div class="input-form">
    <form action="login" method="post">
<p style="color:white;">Login:</p><input type="text" name="login"><br>
</div>
<div class="input-form">
<p style="color:white;">Password:</p><input type="password" name="password"></br>
</div>
<div class="input-form">
<p style="color:white;"><a href="/registration">Registration</a></p>

<c:if test="${pass}">
        <p style="color:white;">Incorrect login or password</p>
</c:if>

<c:if test="${ban}">
        <p style="color:white;">You are banned</p>
</c:if>

<input type="submit" value="Login">
</div>

<c:out value="${user}"/>

<div></br></div>
</body>
</html>
