package entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;


@Data
@AllArgsConstructor
@Builder
public class User {
    private Long id;
    private String login;
    private String password;
    private String email;
    private Role role;
    private Boolean isBanned;
    private BigDecimal cash;

}
