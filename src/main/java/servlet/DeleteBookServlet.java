package servlet;

import dao.impl.BookDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/delete")
public class DeleteBookServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDAOImpl bookDAO = new BookDAOImpl();
        String idBook = req.getParameter("book_id");
        Long longIdBooks = new Long(idBook);
        bookDAO.deleteById(longIdBooks);
        resp.sendRedirect("/main");
    }
}
