package servlet;

import dao.impl.UserDAOImpl;
import dao.interfaces.UserDAO;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class Login extends HttpServlet {

    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = userDAO.findByLogin(login).orElse(null);
        if (user != null && user.getLogin().equals(login) && user.getPassword().equals(password) && !user.getIsBanned()) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            session.setAttribute("userCash", user.getCash());
            session.setAttribute("userId", user.getId());
            session.setAttribute("login", login);
            session.setAttribute("password", password);
            session.setAttribute("isLogged", user);
            session.setAttribute("userRole", user.getRole());
            session.setAttribute("lang", "en");
            if (user.getRole().getRole().equals("admin")) {
                session.setAttribute("isAdmin", true);
            }
            resp.sendRedirect("/about");
        } else if (user != null && user.getIsBanned()) {
            req.setAttribute("ban", true);
            //resp.sendRedirect("/login");
            req.getRequestDispatcher("login.jsp").include(req, resp);
        } else {
            req.setAttribute("pass", true);
            //resp.sendRedirect("/login");
            req.getRequestDispatcher("login.jsp").include(req, resp);
        }

    }
}
