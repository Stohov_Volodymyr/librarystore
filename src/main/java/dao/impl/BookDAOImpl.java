package dao.impl;

import dao.interfaces.BookDAO;
import entity.Book;
import entity.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookDAOImpl implements BookDAO {
    public static final String SELECT_FROM_LIBRARY_BOOK_ID = "SELECT * FROM library_store.books where idbooks = ?;";
    private final Connection connection = ConnectionPool.getConnection();
    public static final String SELECT_FROM_LIBRARY_BOOK = "SELECT * FROM library_store.books;";
    public static final String FIND_ALL_BY_USER = "SELECT idbooks, title, author, cost, themes_id FROM library_store.user_book as ub join books as b on b.idbooks = ub.books_idbooks where ub.users_id = ?;";
    public static final String FIND_ALL_BY_USER_TRANSLATE = "SELECT idbooks, BTS.title, BTS.author, b.cost, b.themes_id FROM books as b\n" +
            "join book_translate as BTS on b.idbooks = BTS.bookId\n" +
            "join language as l on BTS.language_idlanguage = l.idlanguage\n" +
            "where l.langCode = ?";
    public static final String FIND_ALL_BY_USER_TRANSLATE_NEW = "SELECT idbooks, BTS.title, BTS.author, b.cost, b.themes_id FROM library_store.user_book as ub\n" +
            "join books as b on b.idbooks = ub.books_idbooks\n" +
            "join book_translate as BTS on b.idbooks = BTS.bookId\n" +
            "join language as l on BTS.language_idlanguage = l.idlanguage\n" +
            "where ub.users_id = ? && l.langCode = ?;";
    public static final String UPDATE_BOOK = "Update books\n" +
            "SET title = ?, author = ?, cost = ?\n" +
            "WHERE idbooks = ?;";
    public static final String GESHA_TOXIC = "DELETE FROM books WHERE idbooks=?";

    @Override
    public Optional<Book> findById(Long aLong) {
        String sql = SELECT_FROM_LIBRARY_BOOK_ID;

        Book book = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, aLong);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                book = new Book(
                        resultSet.getLong("idbooks"),
                        resultSet.getString("title"),
                        resultSet.getString("author"),
                        resultSet.getBigDecimal("cost"),
                        resultSet.getString("themes_id"));
            }
            return Optional.ofNullable(book);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> findALL() {
        String sql = SELECT_FROM_LIBRARY_BOOK;
        List<Book> books = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                books.add(
                        new Book(resultSet.getLong("idbooks"),
                                resultSet.getString("title"),
                                resultSet.getString("author"),
                                resultSet.getBigDecimal("cost"),
                                resultSet.getString("themes_id")));
            }
            return books;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public List<Book> findALL(String lang) {
        String sql = FIND_ALL_BY_USER_TRANSLATE;
        List<Book> books = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, lang);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                books.add(
                        new Book(resultSet.getLong("idbooks"),
                                resultSet.getString("title"),
                                resultSet.getString("author"),
                                resultSet.getBigDecimal("cost"),
                                resultSet.getString("themes_id")));
            }
            return books;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    @Override
    public Book create(Book entity) {
        return null;
    }

    @Override
    public Book update(Book book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BOOK)) {
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setBigDecimal(3, book.getCost());
            preparedStatement.setLong(4, book.getIdbooks());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return findById(book.getIdbooks()).get();
    }

    @Override
    public void deleteById(Long id) {
        String sql = GESHA_TOXIC;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Book entity) {

    }

    @Override
    public List<Book> findAllByUser(Long id) {
        String sql = FIND_ALL_BY_USER;
        List<Book> books = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                books.add(
                        new Book(resultSet.getLong("idbooks"),
                                resultSet.getString("title"),
                                resultSet.getString("author"),
                                resultSet.getBigDecimal("cost"),
                                resultSet.getString("themes_id")));
            }
            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    public List<Book> findAllByUser(Long id, String aaaaaaa) {
        String sql = FIND_ALL_BY_USER_TRANSLATE_NEW;
        List<Book> books = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.setString(2, aaaaaaa);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                books.add(
                        new Book(resultSet.getLong("idbooks"),
                                resultSet.getString("title"),
                                resultSet.getString("author"),
                                resultSet.getBigDecimal("cost"),
                                resultSet.getString("themes_id")));
            }
            return books;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
}