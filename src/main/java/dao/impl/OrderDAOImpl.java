package dao.impl;

import dao.interfaces.OrderDAO;
import entity.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderDAOImpl implements OrderDAO {

    private Connection connection;

    public OrderDAOImpl() {
        this.connection = ConnectionPool.getConnection();
    }


    @Override
    public Boolean orderBook(Long bookId, Long userId) {
        String sql = "insert into user_book (users_id, books_idbooks) values (?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
