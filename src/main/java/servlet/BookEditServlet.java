package servlet;

import dao.impl.BookDAOImpl;
import entity.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/edit")
public class BookEditServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDAOImpl bookDAO = new BookDAOImpl();
        String cost = req.getParameter("cost");
        BigDecimal bigDecimal = new BigDecimal(cost);
        String idbooks = req.getParameter("editBookId");
        Long longIdBooks = new Long(idbooks);

        Book book = Book.builder()
                .author(req.getParameter("author"))
                .cost(bigDecimal)
                .idbooks(longIdBooks)
                .title(req.getParameter("title")).build();
        bookDAO.update(book);
        resp.sendRedirect("/main");
    }
}