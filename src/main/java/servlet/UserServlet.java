package servlet;

import dao.impl.UserDAOImpl;
import entity.Book;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private BookService bookService = new BookService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UserDAOImpl userDAOImpl = new UserDAOImpl();
        User userId = userDAOImpl.findById(Long.valueOf(String.valueOf(session.getAttribute("userId")))).orElseThrow(RuntimeException::new);
        req.setAttribute("user", userId);
        String str = String.valueOf(session.getAttribute("lang"));
        List<Book> bookLists = bookService.findAllUser(Long.valueOf(String.valueOf(session.getAttribute("userId"))), str);
        req.setAttribute("bookList", bookLists);
        //считает цену всех книг
        req.setAttribute("totalPrice", bookLists.stream().map(book -> book.getCost()).reduce(BigDecimal.ZERO, BigDecimal::add));

        req.getRequestDispatcher("user.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        super.doPost(req, resp);
    }
}
