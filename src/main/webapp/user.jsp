<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/directive/lang.jspf" %>
<%@ page isELIgnored="false" %>
<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">

    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ECE9E0;
            color: #656665;
            border: 5px solid #ECE9E0;
            border-radius: 15px;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #F5D7BF;
            padding: 5px;
        }
    </style>

</head>
<body>

<%@ include file="/WEB-INF/directive/header.jspf" %>

<table border="1" align="center" width="70%">
        <tr>
            <td width="20%">
                <p align="center"><img src="user.png" width="70%"></p>
            </td>
            <td>
                <p><fmt:message key="user_jsp.login"/>&nbsp<c:out value="${user.getLogin()}"/><p>
                <p><fmt:message key="user_jsp.email"/>&nbsp<c:out value="${user.getEmail()}"/><p>
                <p><fmt:message key="user_jsp.cost"/>&nbsp<c:out value="${totalPrice}"/><fmt:message key="user_jsp.grn"/><p>
                <p><fmt:message key="user_jsp.cash"/><c:out value="${user.getCash()}"/><fmt:message key="user_jsp.grn"/><p>

                <form action="magic" method="post">
                    <input type="hidden" name="cash_add" value ="-100">
                    <input type="submit" value="+100">
                </form></td>

    </table>
    </td>
    <tr>
        </table>


<c:forEach var="book" items="${bookList}">
            <table border="1" align="center" width="70%">
                <tr>
                    <td width="30%"><c:out value="${book.title}"/></td>
                    <td width="15%"><c:out value="${book.getAuthor()}"/></td>
                </tr>
            </table>
</c:forEach>


</body>
</html>
