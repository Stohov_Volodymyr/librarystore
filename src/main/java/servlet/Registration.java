package servlet;

import dao.impl.UserDAOImpl;
import dao.interfaces.UserDAO;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class Registration extends HttpServlet {
    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        if (login == null || password == null || email == null || login.isEmpty() || password.isEmpty() || email.isEmpty()) {
            req.getRequestDispatcher("meme.jsp");
        }
        if (userDAO.findByLogin(login).isPresent()) {
            throw new RuntimeException();
        }
        if (userDAO.findByEmail(email).isPresent()) {
            throw new RuntimeException();
        }
        User user = User.builder()
                .login(login)
                .password(password)
                .email(email)
                .role(null)
                .isBanned(false)
                .build();
        userDAO.create(user);
        resp.sendRedirect("/main");
    }
}
