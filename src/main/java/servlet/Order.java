package servlet;

import dao.impl.OrderDAOImpl;
import dao.impl.UserDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/order")
public class Order extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OrderDAOImpl orderDAOImpl = new OrderDAOImpl();
        UserDAOImpl userDAOImpl = new UserDAOImpl();
        HttpSession session = req.getSession();
        Long userId = Long.valueOf(String.valueOf(session.getAttribute("userId")));
        Long bookId = Long.valueOf(req.getParameter("book_id"));
        BigDecimal bookCost = new BigDecimal(String.valueOf(req.getParameter("book_cost")));

        //чтобы деньги в - не ушли
        BigDecimal userCash = new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(bookCost);
        if (userCash.compareTo(new BigDecimal("0.00")) < 0) {
            resp.sendRedirect("/main");
            return;
        }

        orderDAOImpl.orderBook(bookId, userId);
        userDAOImpl.updateCash(userId, bookCost);
        session.setAttribute("userCash", new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(bookCost));
        resp.sendRedirect("/main");
    }
}