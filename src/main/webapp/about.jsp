<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/WEB-INF/directive/lang.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
    <style type="text/css">
            table {
                font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
                text-align: left;
                font-size: 20px;
                border-collapse: separate;
                border-spacing: 5px;
                background: #ECE9E0;
                color: #656665;
                border: 5px solid #ECE9E0;
                border-radius: 15px;
            }

            th {
                font-size: 18px;
                padding: 10px;
            }

            td {
                background: #F5D7BF;
                padding: 5px;
            }
        </style>

</head>
<body>
<div>
<%@ include file="/WEB-INF/directive/header.jspf" %>
<form action="main" method="post">
<input type="submit" name="lang" value="ru">
<input type="submit" name="lang" value="en">
</form>

<table width="90%" align="center"><tr><td><fmt:message key="about_jsp.library"/>
</td></tr></table>

</div>
</body>
</html>