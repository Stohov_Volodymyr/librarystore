package dao.impl;

import dao.interfaces.UserDAO;
import entity.ConnectionPool;
import entity.Role;
import entity.User;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UserDAOImpl implements UserDAO {
    public static final String SELECT_FROM_LIBRARY_USERS_ID = "SELECT user.id, user.login, user.password, user.email, user.isBanned, user.cash, roles.role as role, roles.id as role_id FROM users as user\n" +
            "join roles on user.roles_id = roles.id\n" +
            "WHERE user.id = ?;";
    private final Connection connection = ConnectionPool.getConnection();
    public static final String SELECT_FROM_USER_WHERE_USER_LOGIN = "SELECT user.id, user.login, user.password, user.email,user.isBanned, user.cash, roles.role as role, roles.id as role_id FROM users as user\n" +
            "join roles on user.roles_id = roles.id\n" +
            "WHERE user.login = ?;";
    public static final String SELECT_FROM_USER_WHERE_USER_EMAIL = "SELECT user.id, user.login, user.password, user.email,user.isBanned, user.cash, roles.role as role, roles.id as role_id FROM users as user\n" +
            "join roles on user.roles_id = roles.id\n" +
            "WHERE user.email = ?;";
    public static final String SELECT_ALL_FROM_USERS = "SELECT u.id, login, email, isBanned, roles.role as role, u.cash, roles.id as role_id FROM library_store.users as u\n" +
            "inner join roles on roles.id = u.roles_id;";
    public static final String UPDATE_STATUS_USER = "UPDATE users\n" +
            "SET isBanned = ?\n" +
            "WHERE id = ?;\n";
    public static final String UPDATE_CASH_USER = "update users set cash = cash - ? where id = ?;";


    @Override
    public Optional<User> findById(Long aLong) {
        String sqlId = SELECT_FROM_LIBRARY_USERS_ID;
        String sqlEmail = SELECT_FROM_USER_WHERE_USER_EMAIL;

        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlId)) {
            preparedStatement.setLong(1, aLong);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("email"),
                        new Role(resultSet.getLong("id"), resultSet.getString("role")),
                        resultSet.getBoolean("isBanned"),
                        resultSet.getBigDecimal("cash")
                );
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> findALL() {
        String sql = SELECT_ALL_FROM_USERS;
        List<User> users = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                users.add(
                        new User(
                                resultSet.getLong("id"),
                                resultSet.getString("login"),
                                null,
                                resultSet.getString("email"),
                                new Role(resultSet.getLong("id"), resultSet.getString("role")),
                                resultSet.getBoolean("isBanned"),
                                resultSet.getBigDecimal("cash")));
            }
            return users;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User create(User entity) {
        String sqlCreate = "insert into users(login, email, password ,roles_id, isBanned,cash) values(?,?,?,?,?,0)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlCreate)) {
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getEmail());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setLong(4, 1);
            preparedStatement.setBoolean(5, entity.getIsBanned());
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public User update(User entity) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public Optional<User> findByLogin(String login) {
        String sql = SELECT_FROM_USER_WHERE_USER_LOGIN;
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("email"),
                        new Role(resultSet.getLong("role_id"), resultSet.getString("role")),
                        resultSet.getBoolean("isBanned"),
                        resultSet.getBigDecimal("cash"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        String sqlEmail = SELECT_FROM_USER_WHERE_USER_EMAIL;
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlEmail)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("email"),
                        new Role(resultSet.getLong("role_id"), resultSet.getString("role")),
                        resultSet.getBoolean("isBanned"),
                        resultSet.getBigDecimal("cash"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateStatus(Long userId, Boolean isBanned) {
        String sql = UPDATE_STATUS_USER;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setBoolean(1, isBanned);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public void updateCash(Long id, BigDecimal cash) {
        String sql = UPDATE_CASH_USER;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setBigDecimal(1, cash);
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


}
