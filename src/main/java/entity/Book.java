package entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@Builder
@Data
@AllArgsConstructor
public class Book {
    private Long idbooks;
    private String title;
    private String author;
    private BigDecimal cost;
    private String theme;
}
