package dao.interfaces;

import entity.User;

import java.math.BigDecimal;
import java.util.Optional;

public interface UserDAO extends GenericDAO<User, Long> {

    Optional<User> findByLogin(String login);
    Optional<User> findByEmail(String email);

    void updateStatus(Long userId, Boolean isBanned);

    void updateCash(Long userId, BigDecimal cash);
}

