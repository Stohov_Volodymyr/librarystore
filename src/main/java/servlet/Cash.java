package servlet;

import dao.impl.UserDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/magic")
public class Cash extends HttpServlet {
    UserDAOImpl userDAOImpl = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Long userId = Long.valueOf(String.valueOf(session.getAttribute("userId")));
        BigDecimal cash_add = new BigDecimal(String.valueOf(req.getParameter("cash_add")));
        userDAOImpl.updateCash(userId, cash_add);
        session.setAttribute("userCash", new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(cash_add));
        resp.sendRedirect("/user");
    }
}
