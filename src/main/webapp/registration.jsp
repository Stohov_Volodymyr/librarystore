<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/directive/lang.jspf" %>
<%@ page isELIgnored="false" %>
<!doctype html>
<html lang="${lang}">
<head><meta charset="UTF-8">
<meta name="viewport"
content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Document</title>
 <link rel="stylesheet" href="style.css">

 <style type="text/css">
             table {
                 font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
                 text-align: left;
                 font-size: 20px;
                 border-collapse: separate;
                 border-spacing: 5px;
                 background: #ECE9E0;
                 color: #656665;
                 border: 5px solid #ECE9E0;
                 border-radius: 15px;
             }

             th {
                 font-size: 18px;
                 padding: 10px;
             }

             td {
                 background: #F5D7BF;
                 padding: 5px;
             }
         </style>

</head>

<body>


<%@ include file="/WEB-INF/directive/header.jspf" %>


<div class="form">
<h1>Registration</h1>
<div class="input-form">
<form action="registration" method="post">
<p style="color:white;">Login:</p><input type="text" name="login"><br>
</div>
<div class="input-form">
<p style="color:white;">Email:</p><input type="email" name="email"></br>
</div>
<div class="input-form">
<p style="color:white;">Password:</p><input type="password" name="password"></br>
</div>
<div class="input-form">
<p style="color:white;"><a href="/login">Have account? Login</a></p>
<input type="submit" value="registration">
</div>
</form>
</body>


</html>
