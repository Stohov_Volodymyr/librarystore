package dao.interfaces;

import entity.Book;

import java.util.List;

public interface BookDAO extends GenericDAO<Book, Long> {
    List<Book> findAllByUser(Long id);
}