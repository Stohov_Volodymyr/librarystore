package servlet;

import dao.impl.BookDAOImpl;
import entity.Book;

import java.util.List;

public class BookService {

    private final BookDAOImpl bookDAO = new BookDAOImpl();


    public List<Book> findAll(String langSelect) {
        List<Book> books;
        if (langSelect.equals("en")) {
            books = bookDAO.findALL();

        } else {
            books = bookDAO.findALL(langSelect);
        }
        return books;
    }

    public List<Book> findAllUser(Long id, String langSelection) {
        List<Book> books;
        if (langSelection.equals("en")) {
            books = bookDAO.findAllByUser(id);

        } else {
            books = bookDAO.findAllByUser(id, langSelection);
        }
        return books;
    }
}
